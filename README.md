# README #

Application: Math Exercise
Project Name: MathMania

What does the application do?
 
1. The application is used to practice maths.  It involves five operations addition, subtraction, multiplication, division and exponentiation.
2. It helps you choose 1 or more operators you want to work with.
3. You will give in a number of your choice and get a random number and one of the operations you have selected.
4. Calculate your answer and enter to check answer.
5. You can try as many problems as you want.
6. You can change the operators you want to work with at any time.

What should the user do?
1. The first page is the Home screen, you need to click on the button “Click here to start!”
2. This leads you to an Operators page where you need to choose what operations you wanna practice (addition,subtraction,multiplication,division,power of). You can choose one or more operators.
3. Enter a number of your choice and click “Submit”. The number can be an integer or a decimal number
4. You will be thrown a problem with your choice of number, one of the operations you have chosen and a random number
5. Calculate the answer, enter it and click “Check Answer”. If your answer is right, it will display a message that your answer is correct else it will say it is wrong and show you the correct answer.
6. You can continue this exercise by clicking “Try another problem!”
7. You can change your choice of operators anytime by clicking on “Change Operators”

What are the restrictions in the application?
1. You must choose at least one operation to continue to do a problem
2. User has to input a number. No non-numeric number, space or empty input will work
3. The answer should not be non-numeric, space or empty
4. The answers are not rounded.

What are the languages used?
1. HTML
2. CSS
3. Javascript

What is the structure of the code?
1. Page1 → Home screen  CSS → StylePage1 (Home Screen)
2. Page2 → Home screen  CSS → StylePage2 (Choose Operators)
3. Page3 → Home screen  CSS → StylePage3 (Do the math)